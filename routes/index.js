const express = require('express')
const router = express.Router()

const pp = o => JSON.stringify(o, null, 2)

const buildJsonMessage = req => {
  try {
    return `JSON Body:\n${pp(JSON.parse(req.body))}`
  } catch (e) {
    return `JSON ERROR:\n${e.toString()}`
  }
}

router.post('/*', function (req, res, next) {
  const response = `
Headers: ${pp(req.headers)}

Raw Body:
${req.body}

${buildJsonMessage(req)}
`
  console.log(response)
  res.send(response)
})

module.exports = router
