const express = require('express')
const bodyParser = require('body-parser')
const logger = require('morgan')

const indexRouter = require('./routes/index')

const app = express()

app.use(logger('dev'))
app.use(bodyParser.text({ type: '*/*' }))

app.use('/', indexRouter)

module.exports = app
